package com.bourntec.patientspro.DTO.Request;

import com.bourntec.patientspro.model.Patient;

import lombok.Getter;
//import lombok.Setter;
@Getter

public class PatientRequestDTO {
	
	String name;
	Integer age;
	String doctorName;
	
	public  Patient convertToModel()
	{
		Patient patient=new Patient();
		
		patient.setName(name);
		patient.setAge(age);
		patient.setDoctorName(doctorName);
		return patient;
	}

}
