package com.bourntec.patientspro.model;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Scanner;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.opencsv.bean.CsvDate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Entity
@Table(name="patient")

@EntityListeners(AuditingEntityListener.class)
@Data
public class Patient 
	{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Getter
	@Setter
	//@AllArgsConstructor
	//@NoArgsConstructor
	
	    private Integer id;
	@Column(length=50,nullable=false)
		private String name;
		private Integer age;
		private String doctorName;
		@Enumerated(EnumType.STRING)
		private Gender gender;
@CsvDate(value="yyyy-MM-dd")
		LocalDate dateOfBirth;
		
		@LastModifiedDate
		LocalDateTime lastModifiedDate;
		@CreatedDate
		LocalDateTime createdDate;
		
		
		
		
      
	

		
		
		
}
