package com.bourntec.patientspro.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bourntec.patientspro.DTO.Request.PatientRequestDTO;
import com.bourntec.patientspro.model.Gender;
import com.bourntec.patientspro.model.Patient;
import com.bourntec.patientspro.service.PatientDetails;

@Repository
public interface PatientRepository extends  JpaRepository<Patient,Integer>,JpaSpecificationExecutor<Patient>
{

		List<Patient>findByName(String name);
		//List<Patient>findByAge(int age);
		List<Patient>findByNameAndAge(String name,int age);
		List<Patient>findByNameOrAge(String name,int age);
		//List<Patient>findByAgeGreaterThan(int age);
		List<Patient>findByAgeBetween(int age1,int age2);
	    List<Patient> findByNameLike(String name);
		List<Patient>findByNameOrderByAgeAsc(String name);
		List<Patient>findByNameStartingWith(String name);
		List<Patient>findByNameIgnoreCase(String name);
		@Query(value="select * from patient where age>?1",nativeQuery=true)
		List<Patient>findByAgeGreaterThan(int age);
		List<Patient>findByAgeLessThan(int age);
		List<Patient>findByGender(Gender gender);
	    List<Patient>findByGenderLike(Gender gender);
	    List<Patient> findByDoctorNameIsNull();
	    @Modifying
	    @Transactional
	    @Query(value="update patient set name='ammu' where id=?1",nativeQuery=true)
		void  findById1(int id);
	    @Modifying
	    @Transactional
	    @Query(value="delete from patient where age>:age",nativeQuery=true)
	    void deleteByAge(int age);
	    //String deletetByAge(int age);
	    
	  @Query(value="select p  from Patient p")
	    List<PatientDetails>findAllPatientDetails();
	PatientRequestDTO save(PatientRequestDTO patientRequestDTO);
}
