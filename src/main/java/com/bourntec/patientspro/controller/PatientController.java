package com.bourntec.patientspro.controller;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bourntec.patientspro.DTO.Request.PatientRequestDTO;
import com.bourntec.patientspro.model.Gender;
import com.bourntec.patientspro.model.Patient;
import com.bourntec.patientspro.search.SearchRequest;
import com.bourntec.patientspro.service.PatientDetails;
import com.bourntec.patientspro.service.PatientService;
import com.opencsv.exceptions.CsvValidationException;

@RestController
@RequestMapping("/patients")//parent of all methods of the class
public class PatientController {

	@Autowired
	PatientService patientService;
	@GetMapping("/")
	public String getMessage()
	{
		return "hello";
	}
	@GetMapping("/getall")
	public List<Patient> getAll()
	{
		
		
		return  patientService.getAll();
	}
	@GetMapping("/get")
	public List<Patient> findByName(String name)
	{
		
		
		return  patientService.findByName(name);
	
	}

	/*@GetMapping("/getage")
	public List<Patient> findByAge(int age)
	{
		
		
		return  patientService.findByAge(age);
	
	}*/
	@GetMapping("/getageandname")
	public List<Patient>findByNameAndAge(String name,int age)
	{
		return  patientService.findByNameAndAge(name,age);
	}
	@GetMapping("/getageorname")
	public List<Patient>findByNameOrAge(String name,int age)
	{
		return  patientService.findByNameOrAge(name,age);
	}
	@GetMapping("/getbetween")
	List<Patient>findByAgeBetween(int age1,int age2)
	{
		return  patientService.findByAgeBetween(age1,age2);
	}
	@GetMapping("/getnamelike")
	 List<Patient> findByNameLike(String name)
	{
		return  patientService.findByNameLike(name);
	}
	@GetMapping("/getorderby")
	List<Patient>findByNameOrderByAgeAsc(String name)
	{
		return  patientService.findByNameOrderByAgeAsc(name);
	}
/*	@GetMapping("/getgreater")
	List<Patient>findByAgeGreaterThan(int age)
	{
		return  patientService.findByAgeGreaterThan(age);
	}*/
	@GetMapping("/getless")
	List<Patient>findByAgeLessThan(int age)
	{
		return  patientService.findByAgeLessThan(age);
	}
	@GetMapping("/getstartswith")
	List<Patient>findByNameStartingWith(String name)
	{
		return  patientService.findByNameStartingWith(name);
	}
	@GetMapping("/getignorecase")
	List<Patient>findByNameIgnoreCase(String name)
	{
		return  patientService.findByNameIgnoreCase(name);
	}
	@GetMapping("/getgender")
	List<Patient>findByGender(Gender gender)
	{
		return  patientService.findByGender(gender);
	}
	@GetMapping("/getgenderlike")
    List<Patient> findByGenderLike(Gender gender)
	{
		return  patientService.findByGenderLike(gender);
	}
	@GetMapping("/getnull")
    List<Patient> findByDoctorNameIsNull()
	{
		return  patientService.findByDoctorNameIsNull();
	}
	

	@PostMapping("/create")
	public Patient create(@RequestBody PatientRequestDTO patientRequestDTO)
	{
		return patientService.save(patientRequestDTO.convertToModel());
	}
	@PostMapping("/bulk")//()
	public List<Patient> createAll (@RequestBody List<Patient> patientList)
	{
		return patientService.saveAll(patientList);
	}
	
	
	@DeleteMapping("/{id}")
	public String deleteById(@PathVariable int id) 
	{
	return patientService.deleteById(id);
	}
	
	@GetMapping("/getgreatersearch/{age}")
	public List<Patient>findByAgeGreaterThan(@PathVariable int age)
	{
		return  patientService.findByAgeGreaterThan(age);
	}
	@PutMapping("/{id}")
	public void findById1(@PathVariable int id)
	{
	patientService.findById1(id);
	}
	@DeleteMapping("/deleteage/")
	public void deleteByAge( int age)
	{
	 patientService.deletetByAge(age);
	}
	@PostMapping("/search")
	public List<Patient> search(@RequestBody Patient patient)
	{
		return  patientService.search(patient);
		
		
		
	}
	@GetMapping("/{id}")
	public Patient findById(@PathVariable int id)
	{
		return   patientService.findById(id);
	}
	@PostMapping("/dynamic/search")
	public List<Patient>search(@RequestBody SearchRequest searchRequest)
	{
		return  patientService.search(searchRequest);
	}
	

/*@GetMapping("/importcsv")
public List<Patient>importPatientFromCsv() throws CsvValidationException, IOException
{
	return  patientService.importPatientFromCsv();
}

@GetMapping("/patientdetails")
public List<PatientDetails> findAllPatientDetails() 
{
	return  patientService.findAllPatientDetails();
}*/
	
}
