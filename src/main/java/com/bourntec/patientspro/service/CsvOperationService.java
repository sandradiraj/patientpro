package com.bourntec.patientspro.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import com.bourntec.patientspro.model.Patient;
import com.opencsv.exceptions.CsvValidationException;

public interface CsvOperationService {
	
	List<Patient> readCsv(String fileName) throws FileNotFoundException, CsvValidationException, IOException;
	

}
