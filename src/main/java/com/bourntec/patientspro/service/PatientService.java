package com.bourntec.patientspro.service;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.bourntec.patientspro.DTO.Request.PatientRequestDTO;
import com.bourntec.patientspro.model.Gender;
import com.bourntec.patientspro.model.Patient;
import com.bourntec.patientspro.search.SearchRequest;
import com.opencsv.exceptions.CsvValidationException;

public interface PatientService {
	
	
	
	List<Patient> getAll();
	List<Patient> findByName(String name);
	//List<Patient> findByAge(int age);
	List<Patient>findByNameAndAge(String name,int age);
	List<Patient>findByAgeBetween(int age1,int age2);
	List<Patient>findByNameOrAge(String name,int age);
	//List<Patient>findByAgeGreaterThan(int age);
	List<Patient> findByNameLike(String name);
	List<Patient>findByNameOrderByAgeAsc(String name);
	List<Patient>findByNameStartingWith(String name);
    List<Patient>findByNameIgnoreCase(String name);
    List<Patient>findByAgeLessThan(int age);
    List <Patient>findByGender(Gender gender);
    List <Patient>findByGenderLike(Gender gender);
    List<Patient> findByDoctorNameIsNull();
     List<Patient>findByAgeGreaterThan(int age);
	
	Patient save(Patient patientRequestDTO);
	
	List<Patient> saveAll(List<Patient> patientList);
	
	void findById1(int id);
	//List<Patient> findByAge(int age);
	 void deletetByAge(int age);
	 List<Patient> search(Patient patient);
	  String deleteById(int id);
	 
	Patient findById(int id);
	List<Patient> search(SearchRequest searchRequest);
	
	//List<Patient> importPatientFromCsv() throws CsvValidationException, IOException;
	
	 List<PatientDetails> findAllPatientDetails();
}
