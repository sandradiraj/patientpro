package com.bourntec.patientspro.service;

import java.time.LocalDate;
import java.time.Period;


public interface PatientDetails {
	
	String getName();
	Integer getAge();
	LocalDate curDate=LocalDate.now();  
	
	LocalDate getDateOfBirth();
	
	default int getagefromdob()
	{
		if(getDateOfBirth()!=null)
		return Period.between(getDateOfBirth(),curDate).getYears(); 
				
	else
	{
		return 0;
	}
	}
	default Double getdoubleage()
	{
		if(getAge()!=null)
			return getAge()*2.0;
		else
			return null;
	}


}
