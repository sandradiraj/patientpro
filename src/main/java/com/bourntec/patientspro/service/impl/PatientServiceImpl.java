package com.bourntec.patientspro.service.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
//import java.util.logging.Logger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bourntec.patientspro.DTO.Request.PatientRequestDTO;
import com.bourntec.patientspro.exception.RecordNotFoundException;
import com.bourntec.patientspro.model.Gender;
import com.bourntec.patientspro.model.Patient;
import com.bourntec.patientspro.repository.PatientRepository;
import com.bourntec.patientspro.search.GenericSpecification;
import com.bourntec.patientspro.search.SearchRequest;
import com.bourntec.patientspro.service.CsvOperationService;
import com.bourntec.patientspro.service.PatientDetails;
import com.bourntec.patientspro.service.PatientService;
import com.opencsv.exceptions.CsvValidationException;

import lombok.extern.slf4j.Slf4j;
@Service
@Slf4j
public class PatientServiceImpl implements PatientService {

	@Autowired
	PatientRepository patientRepository;
	@Autowired
	CsvOperationService csvoperationservice;
	
	//CsvOperationServiceImpl  csvoperationserviceimpl;
	@Value("${file.path}")
	String filePath;
	static final Logger log=LoggerFactory.getLogger(PatientServiceImpl.class);
	
	
	public List<Patient> findByName(String name)
	{
		return  patientRepository.findByName(name);
	}
	
	
	public List<Patient> getAll()
	{
		System.out.println(filePath);
	return  patientRepository.findAll();
	}
	
	
	
	/*public List<Patient> findByAge(int age)
	{
		return  patientRepository.findByAge(age);
	}*/
	public List<Patient>findByNameAndAge(String name,int age)
	{
		return  patientRepository.findByNameAndAge(name,age);
	}
	public List<Patient>findByAgeBetween(int age1,int age2)
	{
		return  patientRepository.findByAgeBetween(age1,age2);
	}
	public List<Patient>findByNameOrAge(String name,int age)
	{

		return  patientRepository.findByNameOrAge(name,age);
	}
	public List<Patient> findByNameLike(String name)
	{
		return  patientRepository.findByNameLike(name);
	}
	public List<Patient>findByNameOrderByAgeAsc(String name)
	{
		return  patientRepository.findByNameOrderByAgeAsc(name);
	}
	public List<Patient>findByAgeGreaterThan(int age)
	{
		return  patientRepository.findByAgeGreaterThan(age);
	}
	public List<Patient>findByNameStartingWith(String name)
	{
		return  patientRepository.findByNameStartingWith(name);
	}
	public List<Patient>findByNameIgnoreCase(String name)
	{
		return  patientRepository.findByNameIgnoreCase(name);
	}
	public List<Patient>findByAgeLessThan(int age)
	{
		return  patientRepository.findByAgeLessThan(age);
	}
	
	public List<Patient>findByGender(Gender gender)
	{
		return  patientRepository.findByGender(gender);
	}
	public  List<Patient> findByGenderLike(Gender gender)
		{
			return  patientRepository.findByGenderLike(gender);
		}
	public List<Patient> findByDoctorNameIsNull()
	{
			return  patientRepository.findByDoctorNameIsNull();
	}
	public Patient save(PatientRequestDTO patientRequestDTO)
	{
		return  patientRepository.save(patientRequestDTO.convertToModel());
	}
	
	
	
	
	
	@Transactional
	public String deleteById(int id)
	{
	
	if(patientRepository.existsById(id))
	{
		patientRepository.deleteById(id);
		
	return "DELETED!!!!!" ;
	}
	else
	{
	 throw new RecordNotFoundException();
	}
	}
	
	
	public List<Patient> saveAll(List<Patient> patientList)
	{
		return patientRepository.saveAll(patientList);
	}
	@Transactional
	public void findById1(int id)
	{
		 patientRepository.findById1(id);
		 throw new RecordNotFoundException();
	}
	@Override
	public void deletetByAge(int age) {
		 patientRepository.deleteByAge(age);
	}
	public List<Patient> search(Patient patient)
	{
		return  patientRepository.findAll(Example.of(patient));
	}
	
	public Patient findById(int id)
	{
		Optional<Patient> patientOptional=patientRepository.findById(id); 
				if(patientOptional.isPresent())
					return patientOptional.get();
				else
					throw new RuntimeException();
	}
	
	public List<Patient> search(SearchRequest searchRequest)
	{
		
		return patientRepository.findAll(new GenericSpecification(searchRequest));
	}
	
	
	/*private  List<String> getFileList()
	{
		File directpath=new File(filePath);
		String files[]=directpath.list();
		return Arrays.asList(files);
	}*/

	/*@Value("${file.path}")
	String filePath;
	@Override
	public List<Patient> importPatientFromCsv() throws CsvValidationException, IOException {
		
		
		
		List<Patient> patientlist=new ArrayList<>();
		List<String> fileNames=getFileList();
		for(String filename:fileNames )
		{
			try {
				if(filename.endsWith(".csv"))
				patientlist.addAll(saveAll(csvoperationservice.readCsv(filePath+"\\" +fileNames)));
			} catch (FileNotFoundExceptions e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		return patientlist;
		
	
		
	}*/
	


	@Override
	public  List <PatientDetails> findAllPatientDetails() {
		return patientRepository.findAllPatientDetails();
	}


	@Override
	public Patient save(Patient patientRequestDTO) {
		// TODO Auto-generated method stub
		return null;
	}




/*	@Override
	public Patient save(Patient patientRequestDTO) {
		// TODO Auto-generated method stub
		return null;
	}*/
	
	
	
}
