package com.bourntec.patientspro.service.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.bourntec.patientspro.model.Gender;
import com.bourntec.patientspro.model.Patient;
import com.bourntec.patientspro.service.CsvOperationService;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvException;
import com.opencsv.exceptions.CsvValidationException;
@Primary
@Service
public class CsvOperationServiceOpenCVImp implements  CsvOperationService  {

	@Override
	public List<Patient> readCsv(String fileName) throws  IOException  {
		//Reader reader=new BufferedReader(new FileReader(fileName));
		List<Patient>patientlist=new ArrayList<>();	
		CSVReader csvReader;
		
			csvReader = new CSVReaderBuilder(new FileReader(fileName))
					.withSkipLines(1)
					.build();
		

	
		try {
			List<String[]>recordsList=csvReader.readAll();
			
			for(String[] row:recordsList)
			{
				Patient patient=new Patient();
				patient.setName(row[0].toString());
				patient.setAge(Integer.parseInt(row[1]));
				patient.setDoctorName(row[2].toString());
				patient.setGender(Gender.valueOf(row[3]));
				patient.setDateOfBirth(LocalDate.parse(row[4]));
				patientlist.add(patient);
				
				
				
				
			}
			csvReader.close();
				
			
		} catch (IOException | CsvException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/*
		File file = new File(fileName);
		String target="D:\\csv\\sucess\\"+file.getName();
		if (file.renameTo(new File(target))) {

		System.out.println("File moved successfully");
		}
		else {
		System.out.println("Failed to move the file");
		}*/
	
		
		return patientlist;
	}
	
			

		
	}

