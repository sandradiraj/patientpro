package com.bourntec.patientspro.service.impl;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import com.bourntec.patientspro.exception.FileNotFoundExceptions;
import com.bourntec.patientspro.exception.FileParserException;
import com.bourntec.patientspro.model.Patient;
import com.bourntec.patientspro.service.CsvOperationService;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.exceptions.CsvValidationException;

public class CsvOperationServiceOpenCVImplBeanImpl implements CsvOperationService {

	@Override
	public List<Patient> readCsv(String fileName) throws IllegalStateException, FileNotFoundException
	{
		
		List<Patient> patientlist = null;
		try
		{
		patientlist = new CsvToBeanBuilder(new FileReader(fileName)).withType(Patient.class).build().parse();
		}catch (IllegalStateException | FileNotFoundException e) {
			// TODO Auto-generated catch block
			throw new FileParserException("Exception!!!");
		}
	
		return patientlist;
	}
	

} 

