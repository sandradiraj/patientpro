package com.bourntec.patientspro.service.impl;

//import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
//import java.io.Reader;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.bourntec.patientspro.model.Gender;
import com.bourntec.patientspro.model.Patient;
import com.bourntec.patientspro.service.CsvOperationService;
@Service
public class CsvOperationServiceImpl implements CsvOperationService {

	
	//@Value("${file.path}")
	//String filePath;
	@Override
	public List<Patient> readCsv(String fileName) {
		List<Patient> patientList=new ArrayList<Patient>();
		try
		{
		/*Reader reader=new BufferedReader(new FileReader(fileName));
			CSVReader csvReader=new CSVReader(reader);
					List<String[])>recordsList=csvReader.readAll();*/
		FileReader filereader=new FileReader(fileName);
		Scanner sn=new Scanner(filereader);
		
		while(sn.hasNextLine())
		{
			String[] patientDetails=sn.nextLine().split(",");
			Patient patient=new Patient();
			patient.setName(patientDetails[0]);
			patient.setAge(Integer.parseInt(patientDetails[1]));
			patient.setDoctorName(patientDetails[2]);
			patient.setGender(Gender.valueOf(patientDetails[3]));
			patient.setDateOfBirth(LocalDate.parse(patientDetails[4]));
			
		//System.out.println(sn.nextLine());
		patientList.add(patient);
		}
		
	}catch(FileNotFoundException e)
		{
		e.printStackTrace();
		}
		return patientList;

}
}
