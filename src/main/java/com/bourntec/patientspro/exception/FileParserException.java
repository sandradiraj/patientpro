package com.bourntec.patientspro.exception;


	



	import lombok.AllArgsConstructor;
	import lombok.Getter;
	import lombok.NoArgsConstructor;
	import lombok.Setter;



	@Getter
	@Setter
	@AllArgsConstructor
	@NoArgsConstructor
	public class FileParserException extends RuntimeException
	{
	
	String errorMessage;



	}

