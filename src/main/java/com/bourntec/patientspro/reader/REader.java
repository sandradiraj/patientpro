package com.bourntec.patientspro.reader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.bourntec.patientspro.exception.FileParserException;
import com.bourntec.patientspro.service.CsvOperationService;
import com.bourntec.patientspro.service.PatientService;
import com.opencsv.exceptions.CsvValidationException;
	
	@Component
	
	public class REader {
	@Value("${file.path}")
	String filePath;
	@Autowired
   PatientService patientService;
	@Autowired
	CsvOperationService csvOperationService; 
	@Scheduled(cron="${cron.expression}") 
	public void read() throws CsvValidationException, FileNotFoundException, IOException 
	{
		try
		{
		
		
		List<String> fileNames = getFileList();
			for (String fileName : fileNames) {
				if (fileName.endsWith(".csv")) {
						patientService.saveAll(csvOperationService.readCsv(filePath + "\\" + fileName));
						File sourcefile = new File(filePath +"\\" +fileName);
						String targetfile = filePath + "\\sucess\\" + sourcefile.getName(); 
							if (sourcefile.renameTo(new File(targetfile))) {
								System.out.println("File moved Successfully");
							}
							else 
							{
								System.out.println("Failed to move the file");
							}
					} 
				}
			} catch (Exception e) {
					// TODO Auto-generated catch block
					throw new FileParserException("Exception!!!");
					}
			
			}
	private List<String> getFileList()
	{
		File directoryPath = new File(filePath);
		String files[] = directoryPath.list();
		return Arrays.asList(files);
	}
}




