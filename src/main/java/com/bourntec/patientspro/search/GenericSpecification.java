package com.bourntec.patientspro.search;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.bourntec.patientspro.model.Patient;

public class GenericSpecification<T> implements Specification<T> {

	SearchRequest searchRequest;

public GenericSpecification(SearchRequest searchRequest) {
	this.searchRequest=searchRequest;
	
	}

@Override
public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	
	
	switch(searchRequest.getOperation()) {

	case EQUALS:
	return criteriaBuilder.equal(root.get(searchRequest.getField()), searchRequest.getValue());

	case GREATERTHAN :
	return criteriaBuilder.greaterThan(root.get(searchRequest.getField()), searchRequest.getValue());

	case LESSTHAN:
	return criteriaBuilder.lessThan(root.get(searchRequest.getField()), searchRequest.getValue());

	case GREATERTHANOREQUALS:
	return criteriaBuilder.greaterThanOrEqualTo(root.get(searchRequest.getField()), searchRequest.getValue());

	case LESSTHANOREQUALS:
	return criteriaBuilder.lessThanOrEqualTo(root.get(searchRequest.getField()), searchRequest.getValue());

	default:
	return null;
}
}
}
	





