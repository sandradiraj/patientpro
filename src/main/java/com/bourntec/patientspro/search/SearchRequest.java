package com.bourntec.patientspro.search;

public class SearchRequest {
	
	Operation Operation;

	String field,value;
	
	public SearchRequest(com.bourntec.patientspro.search.Operation operation, String field, String value) {
		
		Operation = operation;
		this.field = field;
		this.value = value;
	}

	public Operation getOperation() {
		return Operation;
	}

	public void setOperation(Operation operation) {
		Operation = operation;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}


}
